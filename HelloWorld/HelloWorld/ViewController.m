//
//  ViewController.m
//  HelloWorld
//
//  Created by Vikram on 2/2/17.
//  Copyright © 2017 hello_world. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *hello_label;
@property (weak, nonatomic) IBOutlet UIButton *hellobutton;
@property (weak, nonatomic) IBOutlet UIButton *yesbutton;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _yesbutton.hidden = YES;
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)hellobutton:(id)sender {
    NSString *path = [[NSBundle mainBundle]pathForResource:
                      @"Hello world of YouTube" ofType:@"mp4"];
    moviePlayer = [[MPMoviePlayerViewController
                    alloc]initWithContentURL:[NSURL fileURLWithPath:path]];
    
    [self presentModalViewController:moviePlayer animated:NO];
    _yesbutton.hidden = NO;
    _hellobutton.hidden = YES;
    self.hello_label.text=@"Hello World... Did you like it??";
}

- (IBAction)yesbutton:(id)sender {
    NSString *path = [[NSBundle mainBundle]
                      pathForResource:@"Applause" ofType:@"mp3"];
    audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                   [NSURL fileURLWithPath:path] error:NULL];
    [audioPlayer play];
    self.hello_label.text=@"Thank you!";
}

@end
