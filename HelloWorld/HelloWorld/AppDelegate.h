//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Vikram on 2/2/17.
//  Copyright © 2017 hello_world. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

