//
//  ViewController.h
//  HelloWorld
//
//  Created by Vikram on 2/2/17.
//  Copyright © 2017 hello_world. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>


@interface ViewController : UIViewController
{
    MPMoviePlayerViewController *moviePlayer;
    AVAudioPlayer *audioPlayer;
}

@end

